<?php
$_ = 'define';


/**
 * @Transactions
 */
$_(START_TRANSACTION, 'START TRANSACTION;');
$_(COMMIT, 'COMMIT;');
$_(ROLLBACK, 'ROLLBACK;');

/**
 * @UserDao
 */
$_(GET_ALL_USERS, 'SELECT *
                   FROM users;');
$_(ADD_USER, 'INSERT
              INTO users (user_id,email,password,ref_money,ref_id,role)
              VALUES (?i,?s,?s,?d,?i,?s);');
$_(GET_USER, 'SELECT *
              FROM users
              WHERE user_id = ?i;');
$_(GET_USER_BY_EMAIL_AND_PASSWORD, 'SELECT *
                                    FROM users
                                    WHERE email = ?s AND password = ?s;');
$_(UPDATE_USER, 'UPDATE users
                 SET email = ?s,password = ?s,ref_money = ?d,ref_id = ?i,role = ?s
                 WHERE user_id = ?i;'
);
$_(DELETE_USER, 'DELETE
                 FROM users
                 WHERE user_id = ?i;');

$_(TOP_REF, 'SELECT * FROM (
                            SELECT uu.email, u.ref_id_fk, COUNT( u.vstr_id ) AS cnt
                            FROM visitor AS u INNER JOIN users AS uu ON uu.user_id = u.ref_id_fk
                            WHERE u.ref_id_fk IS NOT NULL
                            GROUP BY u.ref_id_fk
                            ) AS c
                            ORDER BY c.cnt DESC;');

$_(NOT_DOLLARS, 'SELECT u.email as "Email",u.role as "Роль в системе", u.ref_money as "Бонус пригласителю"
                 FROM users AS u
                 WHERE u.user_id NOT IN  (
                                          SELECT o.user_id_fk
                                          FROM (reserve as r
                                            INNER JOIN (exchange_direction AS ed
                                            INNER JOIN `order` AS o ON ed.edir_id = o.edir_id_fk) ON (r.rsrv_id = ed.rsvr_id_from_fk))
                                          WHERE (r.name NOT LIKE "%USD%")) AND u.ref_money>0;'
);
$_(TOP_EXCHANGERS,'SELECT u.email, sum AS max
FROM (
(
SELECT user_id_fk AS uid, SUM( amount_to ) AS sum
FROM  `order`
GROUP BY user_id_fk
) AS c
)
INNER JOIN users AS u ON u.user_id = c.uid
WHERE sum = (
SELECT MAX( sum ) AS sum
FROM (
(

SELECT  SUM( amount_to ) AS sum
FROM  `order`
GROUP BY user_id_fk ) AS c
)
)
');

/**
 * @ReserveDao
 */
$_(GET_ALL_RESERVES, 'SELECT * FROM reserve;');
$_(ADD_RESERVE, 'INSERT
                 INTO reserve (rsvr_id,`name`,`min`,`max`,active,reserve)
                 VALUES (?i,?s,?d,?d,?i,?d);');
$_(GET_EXDIR, 'SELECT ed.*,r.name as to_name,r.reserve as to_reserve,r1.name as from_name,r1.reserve as from_reserve, (ed.to_amount/ed.from_amount) as course
                              FROM (exchange_direction AS ed
                              INNER JOIN reserve AS r ON ed.rsvr_id_to_fk=r.rsrv_id)
                              INNER JOIN reserve AS r1 ON ed.rsvr_id_from_fk=r1.rsrv_id
                              WHERE ed.edir_id = ?i LIMIT 1;');
$_(GET_RESERVE, 'SELECT * 
				 FROM reserve 
				 WHERE rsvr_id = ?i;');
$_(UPDATE_RESERVE, 'UPDATE reserve 
					SET `name` = ?s,`min` = ?d,`max` = ?d,active = ?i,reserve = ?d 
					WHERE rsvr_id = ?i;');
$_(DELETE_RESERVE, 'DELETE 
					FROM reserve 
					WHERE rsvr_id = ?i;');
$_(GET_RESERVES_BY_ACTIVE, 'SELECT * 
							FROM reserve
							WHERE active=?i');
$_(GET_ALL_ACTIVE_DIRECTIONS, 'SELECT ed.*,r.name as to_name,r.reserve as to_reserve,r1.name as from_name,r1.reserve as from_reserve, (ed.to_amount/ed.from_amount) as course
                              FROM (exchange_direction AS ed
                              INNER JOIN reserve AS r ON ed.rsvr_id_to_fk=r.rsrv_id)
                              INNER JOIN reserve AS r1 ON ed.rsvr_id_from_fk=r1.rsrv_id
                              WHERE ed.active = ?i ORDER BY course DESC;');
$_(INSERT_EDIR, 'INSERT INTO exchange_direction (edir_id,date_add,percent,active,from_amount,to_amount,
user_id_fk,rsvr_id_to_fk,rsvr_id_from_fk) VALUES (NULL,"NOW()",?i,?i,?d,?d,?i,?i,?i)');

$_(UPD_EXDIR, 'UPDATE exchange_direction SET active = 0 WHERE rsvr_id_to_fk=?i AND rsvr_id_from_fk=?i');

/**
 * @OrderDao
 */
$_(GET_ALL_ORDERS_BY_STATUS, 'SELECT * FROM `order` WHERE status=?i;');
$_(GET_ALL_ORDERS_BY_STATUS_AND_USER, 'SELECT * FROM `order` WHERE status=?i AND user_id_fk=?i;');
$_(ADD_ORDER, 'INSERT INTO `order` (order_id,user_id_fk,amount_from,amount_to,service_profit,partner_money,
                                    date_add,status,purse_from,purse_to,last_update,edir_id_fk)
                                    VALUES (?i,?i,?d,?d,?d,?d,NOW(),?i,?s,?s,NOW(),?i);');
$_(GET_ORDER, 'SELECT *
               FROM `order`
               WHERE order_id = ?i;');
$_(UPDATE_ORDER, 'UPDATE `order`
                  SET status = ?i,last_update = "NOW()"
                  WHERE order_id = ?i;');
$_(DELETE_ORDER, 'DELETE
                  FROM `order`
                  WHERE order_id = ?i;');

$_(GET_FULL_SUM, 'SELECT sum(amount_to) AS `sum`
                  FROM `order`
                  WHERE status = 1;');


$_(GET_LAST_COMMENTS, 'SELECT *
					   FROM (`comment` AS c
					   INNER JOIN `users` AS u ON c.`user_id`=u.`user_id`)
					   INNER JOIN `order` AS o ON c.`order_id_fk` = o.`order_id`
					   ORDER BY c.`date` DESC
					   LIMIT ?i;');

$_(SELECT_CURRENT_VISITOR, 'SELECT *
                            FROM visitor
                            WHERE ip=?s AND agent=?s');

$_(UPDATE_CURRENT_VISITOR, 'UPDATE `visitor`
                            SET `last_online`=?i
                            WHERE ip=?s AND agent=?s');

$_(INSERT_NEW_VISITOR, 'INSERT
                        INTO `visitor` (`vstr_id`, `agent`, `ip`, `last_online`, `http_referer`, `ref_id_fk`)
                        VALUES (NULL, ?s, ?s, ?i, ?s, ?i);');
?>