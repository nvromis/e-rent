<?php

class Acl
{
    private static $acl = array();

    public static function initAcl()
    {
        $acl = &self::$acl;

        $all = array('guest', 'user', 'admin');
        $logged_in = array('user', 'admin');
        $not_authorizes = array('guest');


        $acl['catalog:index'] =
        $acl['catalog:add'] = $acl['catalog:get'] =
        $acl['catalog:order'] =  $all;

        $acl['catalog:good'] =$acl['catalog:api'] =$logged_in;

        $acl['feedback:index'] = $acl['feedback:add'] = $acl['feedback:order'] = $all;


        $acl['goods:index'] = $acl['goods:api'] = $acl['goods:history'] =  $all;

        $acl['goods:order'] = $acl['goods:add'] =$acl['messages:index'] =$logged_in;

        $acl['index:index'] = $all;


        $acl['logout:index'] = $all;
        $acl['order:index'] = $acl['order:messages'] = $logged_in;
        $acl['profile:index'] = $acl['profile:edit'] = $logged_in;


        $acl['signin:index'] = $not_authorizes;

        $acl['signup:index'] = $acl['signup:activate'] = $not_authorizes;

        /*
                $acl['orders:news'] =$acl['orders:topref'] = $acl['orders:notdollars'] =
                $acl['courses:add']=$acl['orders:index'] = $acl['order:confirm'] = array('admin');
                $acl['login:signup'] =$acl['login:index'] = &$not_authorizes;

                $acl['orders:topexch']=$acl['index:index'] =    $acl['rules:index'] = $acl['statistic:index'] =
                $acl['courses:index'] = $acl['reserves:index'] =  $acl['feedback:index'] =
                $acl['about:index'] = $acl['faq:index'] = $acl['contacts:index'] = &$all;

                $acl['logout:index'] = $acl['profile:index'] =$acl['profile:edit'] =$acl['feedback:add']
                    =$acl['order:add'] = $acl['order:my'] =
                $acl['order:info'] = &$logged_in;*/
    }

    public static function is_allow($role, $path)
    {
        return is_array(self::$acl[$path]) && in_array($role, self::$acl[$path]);
    }
}