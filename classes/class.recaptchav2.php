<?php

/**
 * @author Nikolay
 * A ReCaptchaResponse is returned from checkAnswer().
 */
class ReCaptchaV2
{

    /**
     * @var string
     */
    private static $_SIGN_UP_URL = "https://www.google.com/recaptcha/admin";

    /**
     * @var string
     */
    private static $_SITE_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify?";

    /**
     * @var string
     */
    private static $_VERSION = "php_1.0";

    /**
     * @var string
     */
    private static $_LANG = "ru";

    /**
     * @var
     */
    private $privateKey;
    /**
     * @var
     */
    private $publicKey;
    /**
     * @var null
     */
    protected $_ip = null;

    /**
     * @return mixed
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * @param mixed $privateKey
     * @return ReCaptchaV2
     */
    public function setPrivateKey($privateKey)
    {
        $this->privateKey = $privateKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * @param mixed $publicKey
     * @return ReCaptchaV2
     */
    public function setPublicKey($publicKey)
    {
        $this->publicKey = $publicKey;
        return $this;
    }


    /**
     * Constructor.
     *
     * @param string $secret
     *            shared secret between site and ReCAPTCHA server.
     */
    public function __construct($publicKey = null, $privateKey = null,
                                $options = null, $ip = null)
    {
        if ($publicKey !== null) {
            $this->setPublicKey($publicKey);
        }

        if ($privateKey !== null) {
            $this->setPrivateKey($privateKey);
        }

        if ($ip !== null) {
            $this->setIp($ip);
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $this->setIp($_SERVER['REMOTE_ADDR']);
        }


    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            $return = $this->getHtml();
        } catch (Exception $e) {
            $return = '';
            trigger_error($e->getMessage(), E_USER_WARNING);
        }

        return $return;
    }

    /**
     * @param $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->_ip = $ip;

        return $this;
    }

    /**
     * @return null
     */
    public function getIp()
    {
        return $this->_ip;
    }

    /**
     * Get the HTML code for the captcha
     */
    public function getHtml($name = null)
    {
        return '<center><div class="g-recaptcha" data-sitekey="' . $this->getPublicKey() . '"></div></center>' .
        '<script src="https://www.google.com/recaptcha/api.js?hl=' . self::$_LANG . '"></script>';
    }

    /**
     * Encodes the given data into a query string format.
     *
     * @param array $data
     *            array of string elements to be encoded.
     *
     * @return string - encoded request.
     */
    private function _encodeQS($data)
    {
        $req = "";
        foreach ($data as $key => $value) {
            $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
        }

        // Cut the last '&'
        $req = substr($req, 0, strlen($req) - 1);
        return $req;
    }

    /**
     * Submits an HTTP GET to a reCAPTCHA server.
     *
     * @param string $path
     *            url path to recaptcha server.
     * @param array $data
     *            array of parameters to be sent.
     *
     * @return array response
     */
    private function _submitHTTPGet($path, $data)
    {
        $req = $this->_encodeQS($data);
        $response = file_get_contents($path . $req);
        return $response;
    }

    /**
     * Calls the reCAPTCHA siteverify API to verify whether the user passes
     * CAPTCHA test.
     *
     * @param string $remoteIp
     *            IP address of end user.
     * @param string $response
     *            response string from recaptcha verification.
     *
     * @return ReCaptchaResponse
     */
    public function verify($response)
    {
        // Discard empty solution submissions
        if ($response == null || strlen($response) == 0) {
            $recaptchaResponse = ReCaptchaV2Response();
            $recaptchaResponse->success = false;
            $recaptchaResponse->errorCodes = 'missing-input';
            return $recaptchaResponse;
        }

        $getResponse = $this->_submitHttpGet(self::$_SITE_VERIFY_URL,
            array(
                'secret' => $this->getPrivateKey(),
                'remoteip' => $this->getIp(),
                'v' => self::$_VERSION,
                'response' => $response
            ));
        $answers = json_decode($getResponse, true);
        $recaptchaResponse = new ReCaptchaV2Response();

        if (trim($answers['success']) == true) {
            $recaptchaResponse->success = true;
        } else {
            $recaptchaResponse->success = false;
            $recaptchaResponse->errorCodes = $answers[error - codes];
        }

        return $recaptchaResponse;
    }

}

?>