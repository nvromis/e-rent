<?php

class utils
{

    private static $instance;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    public function visitor_save()
    {
        $db = db::getInstance();
        $ref_id = $_COOKIE['ref'];
        $http_ref = $_SERVER['HTTP_REFERER'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $last_online = time();
        $agent = $_SERVER['HTTP_USER_AGENT'];

        $v = $db->getRow(SELECT_CURRENT_VISITOR, $ip, $agent);
        if (!$v) {
            $db->query(INSERT_NEW_VISITOR, $agent, $ip, $last_online, $http_ref, $ref_id);
        } else {
            $db->query(UPDATE_CURRENT_VISITOR, $last_online, $ip, $agent);
        }
    }
}

?>