<?php

function autoloadClass($className)
{
    $filename = APPLICATION_PATH . "classes/class." . strtolower($className) . ".php";


    if (is_readable($filename)) {
        require $filename;
    }
}

function autoloadController($className)
{
    $filename = APPLICATION_PATH . "controllers/" . strtolower($className) . ".php";

    if (is_readable($filename)) {
        require $filename;
    }
}

function autoloadDao($className)
{
    $filename = APPLICATION_PATH . "dao/" . strtolower($className) . ".php";


    if (is_readable($filename)) {
        require $filename;
    }
}

function autoloadModel($className)
{
    $filename = APPLICATION_PATH . "models/model." . strtolower($className) . ".php";


    if (is_readable($filename)) {
        require $filename;
    }
}

spl_autoload_register("autoloadClass");
spl_autoload_register("autoloadModel");
spl_autoload_register("autoloadDao");
spl_autoload_register("autoloadController");