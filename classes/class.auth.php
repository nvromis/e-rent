<?php

class Auth
{

    public static function setIdentity(stdClass $user)
    {
        if (!self::hasIdentity()) {
            $_SESSION['id'] = $user->id;
            $_SESSION['role'] = $user->role;
        }
    }

    public static function getIdentity()
    {
        if (self::hasIdentity()) {
            $ud = UserDao::getInstance();
            return $ud->getUserById($_SESSION['id']);
        } else return null;

    }

    public static function hasIdentity()
    {
        return isset($_SESSION['id']);
    }

    public static function destroy()
    {
        session_destroy();
    }
}
