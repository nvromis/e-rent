<?php
class Application
{
    public static function run($url)
    {
		if ($url[strlen($url) - 1] != "/"&&$url!="") {
            header("Location: /$url/",true,301);
            die("Redirect");
        }
		
	//var_dump($url);exit;
        list($class, $method, $param_string) = explode('/', $url, 3);
        $params = array();
        while ($param_string != "") {
            list($key, $value, $param_string) = explode('/', $param_string, 3);
            $params[$key] = $value != null && $value != "" ? $value : 0;
        }
        $class = $class != "" && $class != null ? $class : 'index';
        $method = $method != "" && $method != null ? $method : 'index';

        $view_dir = $controller = $class;
        $front = array('FrontController', 'render');
        if (class_exists($class .= 'Controller')) {
            $obj = new $class();
            if (method_exists($obj, $method)) {
                Acl::initAcl();
                $role = is_null(Auth::getIdentity()) ? 'guest' : Auth::getIdentity()->role;
                if (Acl::is_allow($role, $controller . ':' . $method)) {
                    $call = array($obj, $method);
                    $obj::init();
                    $view = $call($params);
                    $front($view, $view_dir, $method);
                } else if ($role == 'guest') {

                    $front(array('set'=>array('layout' => false, 'redirect' => '/')), 'error', '403');
                } else $front(array('set'=>array('layout' => false)), 'error', '403');


            } else $front(array('set'=>array('layout' => false)), 'error', '404');
        } else $front(array('set'=>array('layout' => false)), 'error', '404');

    }
}