<?php

class config
{

    private static $_conf=array();
    private static $instance;

    private function __construct()
    {
        self::init();
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    private static function init()
    {
        self::$_conf = parse_ini_file(APPLICATION_PATH.'configs/application.ini',true);

    }
    public static function get($param){
        if (!in_array($param,array_keys(self::$_conf))){
            throw new Exception('Invalid!');
        }
        return (object)self::$_conf[$param];
    }

    public $HostDB = "localhost";
    public $UserDB = "root";
    public $PassDB = "";
    public $BaseDB = "obminka";

}
