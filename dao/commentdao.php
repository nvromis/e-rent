<?php

class CommentDao extends Dao
{

    private static $_instance;

    private function __construct()
    {
        parent::init();
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getLastComments($count=12)
    {
        $count = (int)$count;
        $rows = self::$db->getAll(GET_LAST_COMMENTS,$count);
        if (!$rows) {
            return null;
        }

        return $rows;
    }
    public function getAllComments()
    {
        $rows = self::$db->getAll(GET_ALL_COMMENTS);
        if (!$rows) {
            return null;
        }
        foreach ($rows as $u) {
            $Comment = new Comment();
            $Comments[] = $Comment->exchangeArray($u);
        }
        return $Comments;
    }

    public function getComment($id)
    {
        $id = (int)$id;
        $row = self::$db->getRow(GET_COMMENT, $id);
        if (!$row) {
            return null;
        }
        $Comment = new Comment();
        return $Comment->exchangeArray($row);
    }

    public function addComment(Comment $Comment)
    {

        //($Comment_id,$email,$password,$ref_money,$ref_id,$role);
        self::$db->query(START_TRANSACTION);
        $q = self::$db->query(ADD_COMMENT, $Comment->getCommentId(), $Comment->getEmail(), $Comment->getPassword(), $Comment->getRefMoney(), $Comment->getRefId(), $Comment->getRole());
        if ($q) {
            self::$db->query(COMMIT);
            return self::$db->LastInsert();
        } else
            self::$db->query(ROLLBACK);

        return $q;
    }

    public function updateComment(Comment $Comment)
    {

        //($Comment_id,$email,$password,$ref_money,$ref_id,$role);
        self::$db->query(START_TRANSACTION);
        $q = self::$db->query(UPDATE_COMMENT, $Comment->getEmail(), $Comment->getPassword(), $Comment->getRefMoney(), $Comment->getRefId(), $Comment->getRole(), $Comment->getCommentId());
        if ($q) {
            self::$db->query(COMMIT);
        } else
            self::$db->query(ROLLBACK);

        return $q;
    }

    public function deleteComment($id)
    {

        $id = (int)$id;
        self::$db->query(START_TRANSACTION);
        $q = self::$db->query(DELETE_COMMENT, $id);
        if ($q) {
            self::$db->query(COMMIT);
        } else
            self::$db->query(ROLLBACK);
        return $q;
    }


}

