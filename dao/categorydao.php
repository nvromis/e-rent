<?php

class CategoryDao extends Dao
{

    private static $_instance;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        parent::init();
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getAllCategories()
    {
        $rows = self::$db->getAll('SELECT * FROM category ORDER BY name DESC');
        if (!$rows) {
            return null;
        }

        return array_map(function ($el) {
            return (object)$el;
        }, $rows);
    }



    public function getCategory($id)
    {
        $row = self::$db->getRow('SELECT * FROM category WHERE category_id=?s', $id);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function addCategory($category)
    {
        $sql = "INSERT INTO `category` (`category_id`, `name`, `description`) VALUES (NULL, ?s, ?s);";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $category->name, $category->description);
        if ($q) {
            self::$db->query('COMMIT');
            return self::$db->last_insert_id();
        } else
            self::$db->query('ROLLBACK');
        return $q;
    }


    public function deleteCategory($id)
    {

        $sql = "DELETE FROM `category` WHERE category_id=?s;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $id);
        if ($q) {
            self::$db->query('COMMIT');
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }


}

