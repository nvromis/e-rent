<?php

class ReserveDao extends Dao
{

    private static $_instance;

    private function __construct()
    {
        parent::init();
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getAllDirectionsByActive($active = 0)
    {
        $rows = self::$db->getAll(GET_ALL_ACTIVE_DIRECTIONS, $active);
        if (!$rows) {
            return null;
        }
        return $rows;
    }


    public function getReservesByActive($active = 0)
    {
        $rows = self::$db->getAll(GET_RESERVES_BY_ACTIVE, $active);
        if (!$rows) {
            return null;
        }
        foreach ($rows as $u) {
            $Reserve = new Reserve();
            $Reserves[] = $Reserve->exchangeArray($u);
        }
        return $Reserves;
    }


    public function getAllReserves()
    {
        $rows = self::$db->getAll(GET_ALL_RESERVES);
        if (!$rows) {
            return null;
        }
        foreach ($rows as $u) {
            $Reserve = new Reserve();
            $Reserves[] = $Reserve->exchangeArray($u);
        }
        return $Reserves;
    }

    public function getExDir($id)
    {
        $id = (int)$id;
        $row = self::$db->getRow(GET_EXDIR,$id);
        if (!$row) {
            return null;
        }
        return $row;
    }
    public function getReserve($id)
    {
        $id = (int)$id;
        $row = self::$db->getRow(GET_RESERVE, $id);
        if (!$row) {
            return null;
        }
        $Reserve = new Reserve();
        return $Reserve->exchangeArray($row);
    }

    public function addReserve(Reserve $Reserve)
    {

        //(rsvr_id,`name`,`min`,`max`,active,reserve)
        self::$db->query(START_TRANSACTION);
        $q = self::$db->query(ADD_RESERVE, $Reserve->getRsvrId(), $Reserve->getName(), $Reserve->getMin(), $Reserve->getMax(), $Reserve->getActive(), $Reserve->getReserve());
        if ($q) {
            self::$db->query(COMMIT);
            return self::$db->LastInsert();
        } else
            self::$db->query(ROLLBACK);

        return $q;
    }

    public function updateReserve(Reserve $Reserve)
    {

        //($Reserve_id,$email,$password,$ref_money,$ref_id,$role);
        self::$db->query(START_TRANSACTION);
        $q = self::$db->query(UPDATE_RESERVE, $Reserve->getName(), $Reserve->getMin(), $Reserve->getMax(), $Reserve->getActive(), $Reserve->getReserve(), $Reserve->getRsvrId());
        if ($q) {
            self::$db->query(COMMIT);
        } else
            self::$db->query(ROLLBACK);

        return $q;
    }

    public function deleteReserve($id)
    {

        $id = (int)$id;
        self::$db->query(START_TRANSACTION);
        $q = self::$db->query(DELETE_RESERVE, $id);
        if ($q) {
            self::$db->query(COMMIT);
        } else
            self::$db->query(ROLLBACK);
        return $q;
    }


}

