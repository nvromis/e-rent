<?php

class UserDao extends Dao
{

    private static $_instance;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        parent::init();
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getAllUsers()
    {
        $rows = $this->db->getAll('SELECT * FROM users');
        if (!$rows) {
            return null;
        }

        return array_map(function ($el) {
            return (object)$el;
        }, $rows);
    }


    public function getUserByEmailAndPassword($email, $password)
    {
        $row = self::$db->getRow('SELECT * FROM users WHERE email=?s AND password=?s', $email, $password);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function getUserById($id)
    {
        $row = self::$db->getRow('SELECT * FROM users WHERE id=?i', (int)$id);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function getUser($email)
    {
        $row = self::$db->getRow('SELECT * FROM users WHERE email=?s', $email);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function addUser($user)
    {
        $sql = "INSERT INTO `users` (`email`, `name`, `surname`, `patronymic`, `password`, `reg_date`,  `role`)
                             VALUES (?s, NULL, NULL, NULL, ?s, 'NOW()',  'guest');";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $user->email, $user->password);
        if ($q) {
            self::$db->query('COMMIT');
            return self::$db->last_insert_id();
        } else
            self::$db->query('ROLLBACK');
        return $q;
    }

    public function updateUser($user)
    {

        $sql = "UPDATE `users` SET `name`=?s, `surname`=?s, `patronymic`=?s, `password`=?s, `role`=?s
                            WHERE email=?s;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $user->name, $user->surname, $user->patronymic, $user->password, $user->role, $user->email);
        if ($q) {
            self::$db->query('COMMIT');
            return $q;
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }

    public function deleteUser($email)
    {

        $sql = "DELETE FROM `users` WHERE email=?s;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $email);
        if ($q) {
            self::$db->query('COMMIT');
            return $q;
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }


}

