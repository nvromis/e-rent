<?php

class PhotoalbumDao extends Dao
{

    private static $_instance;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        parent::init();
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getAllByIdGood($id)
    {
        $row = self::$db->getAll('SELECT * FROM photoalbum WHERE good_id=?i',$id);
        if (!$row) {
            return null;
        }

        return $row;
    }

    public function addPhoto($good_id,$path)
    {
        $sql = "INSERT INTO `photoalbum` (`photoalbum_id`,`good_id` ,`path`) VALUES (NULL, ?i ,?s);";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $good_id,$path);
        if ($q) {
            $id = self::$db->last_insert_id();
            self::$db->query('COMMIT');
            return $id;
        } else
            self::$db->query('ROLLBACK');
        return $q;
    }


    public function deletePhoto($id)
    {

        $sql = "DELETE FROM `photoalbum` WHERE photoalbum_id=?i;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $id);
        if ($q) {
            self::$db->query('COMMIT');
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }


}

