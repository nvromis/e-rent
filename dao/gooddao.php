<?php

class GoodDao extends Dao
{

    private static $_instance;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        parent::init();
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getAllGoods()
    {
        $rows = self::$db->getAll('select *, logo as path, (date>(INTERVAL -3 DAY + CURRENT_TIMESTAMP)) as new from goods_brands_category');
        if (!$rows) {
            return null;
        }

        return array_map(function ($el) {
            return (object)$el;
        }, $rows);
    }

    public function isFree($id)
    {
        $sql='SELECT IF (EXISTS(
SELECT `good_id`
FROM `goods`
WHERE `good_id`=?i AND `good_id` NOT IN (SELECT `good_id`
FROM `orders` AS o
INNER JOIN `goods` g ON g.good_id = o.goods_id
WHERE `return_date` IS NULL)
), 1,0) AS res';
        $rows = self::$db->getRow($sql,$id);

        return $rows['res'];
    }
    public function getMinMax()
    {
        $rows = self::$db->getRow('SELECT MIN(price) as min, MAX(price) as max FROM goods');
        if (!$rows) {
            return null;
        }

        return (object)$rows;
    }
    public function getTopGoods()
    {
        $sql = 'SELECT g.`name`,g.`good_id`,g.`logo` as path, g.`description`, g.`price`, COUNT(`order_id`) AS cnt
FROM `orders` as o
INNER JOIN `goods` g ON o.`goods_id` = g.`good_id`
GROUP BY `good_id`
ORDER BY cnt DESC';
        $rows = self::$db->getAll($sql);

        if (!$rows) {
            return null;
        }
        $res = array_map(function ($el) {
            return (object)$el;
        }, $rows);
        return $res;
    }
    public function getTopGoodsByCategory($id)
    {
        $sql = 'SELECT g.`name`,g.`good_id`,g.`logo` as path, g.`description`, g.`price`, COUNT(`order_id`) AS cnt
FROM `orders` as o
INNER JOIN `goods` g ON o.`goods_id` = g.`good_id`
where category_id=?i
GROUP BY `good_id`
ORDER BY cnt DESC';
        $rows = self::$db->getAll($sql, $id);

        if (!$rows) {
            return null;
        }
        $res = array_map(function ($el) {
            return (object)$el;
        }, $rows);
        return $res;
    }

    public function searchTopGoods($word)
    {
        $sql = 'SELECT g.`name`,g.`good_id`,g.`logo` as path, g.`description`, g.`price`, COUNT(`order_id`) AS cnt
FROM `orders` as o
INNER JOIN `goods` g ON o.`goods_id` = g.`good_id`
where `good_name` LIKE ?s OR `brand` LIKE ?s OR `good_description` LIKE ?s
GROUP BY `good_id`
ORDER BY cnt DESC';
        $rows = self::$db->getAll($sql, '%'.$word.'%','%'.$word.'%','%'.$word.'%');

        if (!$rows) {
            return null;
        }
        $res = array_map(function ($el) {
            return (object)$el;
        }, $rows);
        return $res;
    }

    public function getGoodsByUser($id)
    {
        $sql = 'select *, logo as path, (date>(INTERVAL -3 DAY + CURRENT_TIMESTAMP)) as new from goods_brands_category WHERE user_id=?i LIMIT 4';
        $rows = self::$db->getAll($sql, $id);

        if (!$rows) {
            return null;
        }
        $res = array_map(function ($el) {
            return (object)$el;
        }, $rows);
        //var_dump($res);
        return $res;
    }
    public function getAllGoodsByCategory($id,$limit=0)
    {

        $sql = 'select *, logo as path, (date>(INTERVAL -3 DAY + CURRENT_TIMESTAMP)) as new from goods_brands_category WHERE category_id=?i';
        if ($limit>0){
            $sql=$sql.' limit '.$limit;
        }
        $rows = self::$db->getAll($sql, $id);

        if (!$rows) {
            return null;
        }
        $res = array_map(function ($el) {
            return (object)$el;
        }, $rows);
        //var_dump($res);
        return $res;
    }
    public function searchGoods($word,$limit=0)
    {

        $sql = 'SELECT *, logo as path, (date>(INTERVAL -3 DAY + CURRENT_TIMESTAMP)) as new
FROM `goods_brands_category`
WHERE `good_name` LIKE ?s OR `brand` LIKE ?s OR `good_description` LIKE ?s';
        if ($limit>0){
            $sql=$sql.' limit '.$limit;
        }
        $rows = self::$db->getAll($sql, '%'.$word.'%','%'.$word.'%','%'.$word.'%');

        if (!$rows) {
            return null;
        }
        $res = array_map(function ($el) {
            return (object)$el;
        }, $rows);
        //var_dump($res);
        return $res;
    }


    public function getAllGoodsByUser($id)
    {
        $rows = self::$db->getAll('select * from goods_brands_category WHERE user_id=?i', $id);
        if (!$rows) {
            return null;
        }

        return array_map(function ($el) {
            return (object)$el;
        }, $rows);
    }

    public function getGoodById($id)
    {
        $row = self::$db->getRow('SELECT * FROM goods WHERE good_id=?i', (int)$id);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function getGood($id)
    {
        $row = self::$db->getRow('SELECT * FROM goods_brands_category WHERE good_id=?i', $id);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }
    public function getSimpleGood($id)
    {
        $row = self::$db->getRow('SELECT * FROM goods WHERE good_id=?i', $id);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function addGood($good)
    {
        $sql = "INSERT INTO `goods` (`good_id`, `user_id`,`name`, `description`, `price`, `date`, `category_id`, `brand_id`,`logo`)
          VALUES (NULL,?i, ?s, ?s,?d, 'NOW()', ?i, ?i,?s);";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $good->user_id, $good->name, $good->description, $good->price, $good->category_id, $good->brand_id, $good->logo);
        if ($q) {
            $id = self::$db->last_insert_id();
            self::$db->query('COMMIT');
            return $id;
        } else
            self::$db->query('ROLLBACK');
        return $q;
    }

    public function updateGood($good)
    {

        $sql = "UPDATE `goods` SET `name`=?s, `description`=?s, `price`=?d, `category_id`=?i, `brand_id`=?i, `logo`=?s
                            WHERE good_id=?i;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $good->name, $good->description, $good->price, $good->category_id, $good->brand_id, $good->logo,
            $good->good_id);
        if ($q) {
            self::$db->query('COMMIT');
            return $q;
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }

    public function deleteGood($id)
    {

        $sql = "DELETE FROM `goods` WHERE good_id=?i;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $id);
        if ($q) {
            self::$db->query('COMMIT');
            return $q;
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }


}

