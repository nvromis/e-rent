<?php

class ConfirmationDao extends Dao
{

    private static $_instance;

    private function __construct()
    {
        parent::init();
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function getByHash($hash)
    {
        $row = self::$db->getRow('SELECT * FROM confirmation WHERE hash=?s', $hash);
        if (!$row) {
            return null;
        }
        return (object)$row;
    }


    public function addHash($hash)
    {
        $sql = "INSERT INTO `confirmation` (`user_email`, `hash`, `type`) VALUES (?s,?s,?s);";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $hash->user_email, $hash->hash, $hash->type);
        if ($q) {
            self::$db->query('COMMIT');
            return self::$db->LastInsert();
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }


    public function deleteUser($hash)
    {

        $sql = "DELETE FROM `confirmation` WHERE hash=?s;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $hash);
        if ($q) {
            self::$db->query('COMMIT');
            return self::$db->LastInsert();
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }


}

