<?php

abstract class Dao
{

    protected static $db = null;

    protected static function init()
    {
        self::$db = db::getInstance();
    }

}