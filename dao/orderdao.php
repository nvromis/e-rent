<?php

class OrderDao extends Dao
{

    private static $_instance;

    private function __construct()
    {
        parent::init();
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function getOrdersForMe($email)
    {
        $rows = self::$db->getAll('SELECT * from orders o inner join goods g on g.good_id=o.goods_id
 WHERE landlord=?s',$email);
        if (!$rows) {
            return null;
        }
		
        return array_map(function ($el) {
			unset($el['goods_id']);
            return (object)$el;
        }, $rows);
    }
    public function getMyOrders($email)
    {
        $rows = self::$db->getAll('SELECT * from orders o inner join goods g on g.good_id=o.goods_id
 WHERE tenant=?s',$email);
        if (!$rows) {
            return null;
        }
        return array_map(function ($el) {
			unset($el['goods_id']);
            return (object)$el;
        }, $rows);
    }
    public function getAllOrders($status=0)
    {
        $status = (int)$status;
        $rows = self::$db->getAll(GET_ALL_ORDERS_BY_STATUS,$status);
        if (!$rows) {
            return null;
        }
        foreach ($rows as $u) {
            $order = new Order();
            $orders[] = $order->exchangeArray($u);
        }
        return $orders;
    }
    public function getAllOrdersByUser($status=0,$uid)
    {
        $status = (int)$status;
        $uid = (int)$uid;
        $rows = self::$db->getAll(GET_ALL_ORDERS_BY_STATUS_AND_USER,$status,$uid);
        if (!$rows) {
            return null;
        }
        foreach ($rows as $u) {
            $order = new Order();
            $orders[] = $order->exchangeArray($u);
        }
        return $orders;
    }


    public function getFullSum()
    {
        $row = self::$db->getRow(GET_FULL_SUM);
        if (!$row) {
            return null;
        }
        return $row['sum'];
    }

    public function getOrder($id)
    {
        $id = (int)$id;
        $row = self::$db->getRow(GET_ORDER, $id);
        if (!$row) {
            return null;
        }
        $order = new Order();
        return $order->exchangeArray($row);
    }

    public function addOrder($order)
    {

        self::$db->query('START TRANSACTION');
        self::$db->query('INSERT INTO `orders` (`order_id`, `from_date`, `to_date`, `return_date`, `landlord`, `tenant`, `goods_id`, `status`)
VALUES (NULL, ?s, ?s, NULL, ?s, ?s, ?i, ?s)',$order->from_date,$order->to_date,$order->landlord,$order->tenant,$order->goods_id,$order->status);
        $id=self::$db->last_insert_id();
        self::$db->query('COMMIT');
        return $id;

    }

    public function updateOrder($order)
    {
        self::$db->query('UPDATE `orders` SET `return_date`=?s,  `status`=?s WHERE order_id=?i',$order->return_date,$order->status);
    }

    public function deleteOrder($id)
    {
        $id = (int)$id;
        self::$db->query(START_TRANSACTION);
        $q = self::$db->query(DELETE_ORDER, $id);
        if ($q) {
            self::$db->query(COMMIT);
        } else
            self::$db->query(ROLLBACK);
        return $q;
    }


}

