<?php

class BrandDao extends Dao
{

    private static $_instance;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        parent::init();
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getAllBrands()
    {
        $rows = self::$db->getAll('SELECT *, (select count(*) from goods as g where g.brand_id = b.brand_id) as cnt FROM brands b ORDER BY brand ASC');
        if (!$rows) {
            return null;
        }

        return array_map(function ($el) {
            return (object)$el;
        }, $rows);
    }

    public function getBrandByName($name)
    {
        $row = self::$db->getRow('SELECT * FROM brands WHERE brand LIKE ?s', '%'.$name.'%');
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function getBrand($id)
    {
        $row = self::$db->getRow('SELECT * FROM brands WHERE brand_id=?s', $id);
        if (!$row) {
            return null;
        }

        return (object)$row;
    }

    public function addBrand($brand)
    {
        $sql = "INSERT INTO `brands` (`brand_id`, `brand`) VALUES (NULL, ?s);";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $brand->brand);
        if ($q) {
            self::$db->query('COMMIT');
            return self::$db->last_insert_id();
        } else
            self::$db->query('ROLLBACK');
        return $q;
    }


    public function deleteBrand($id)
    {

        $sql = "DELETE FROM `brands` WHERE brand_id=?i;";
        self::$db->query('START TRANSACTION');
        $q = self::$db->query($sql, $id);
        if ($q) {
            self::$db->query('COMMIT');
        } else
            self::$db->query('ROLLBACK');

        return $q;
    }


}

