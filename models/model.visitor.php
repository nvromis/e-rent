<?php

class Visitor extends Base
{

    protected $vstr_id = null;
    protected $agent;
    protected $ip;
    protected $last_online;
    protected $http_referer;
    protected $ref_if_fk;

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     * @return Visitor
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHttpReferer()
    {
        return $this->http_referer;
    }

    /**
     * @param mixed $http_referer
     * @return Visitor
     */
    public function setHttpReferer($http_referer)
    {
        $this->http_referer = $http_referer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     * @return Visitor
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastOnline()
    {
        return $this->last_online;
    }

    /**
     * @param mixed $last_online
     * @return Visitor
     */
    public function setLastOnline($last_online)
    {
        $this->last_online = $last_online;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRefIfFk()
    {
        return $this->ref_if_fk;
    }

    /**
     * @param mixed $ref_if_fk
     * @return Visitor
     */
    public function setRefIfFk($ref_if_fk)
    {
        $this->ref_if_fk = $ref_if_fk;
        return $this;
    }

    /**
     * @return null
     */
    public function getVstrId()
    {
        return $this->vstr_id;
    }

    /**
     * @param null $vstr_id
     * @return Visitor
     */
    public function setVstrId($vstr_id)
    {
        $this->vstr_id = $vstr_id;
        return $this;
    }

}