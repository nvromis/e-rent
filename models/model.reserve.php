<?php

class Reserve extends Base
{

    protected $rsrv_id = null;
    protected $name;
    protected $min;
    protected $max;
    protected $active;
    protected $reserve;

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return Reserve
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param mixed $max
     * @return Reserve
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param mixed $min
     * @return Reserve
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Reserve
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReserve()
    {
        return $this->reserve;
    }

    /**
     * @param mixed $reserve
     * @return Reserve
     */
    public function setReserve($reserve)
    {
        $this->reserve = $reserve;
        return $this;
    }

    /**
     * @return null
     */
    public function getRsvrId()
    {
        return $this->rsrv_id;
    }

    /**
     * @param null $rsvr_id
     * @return Reserve
     */
    public function setRsvrId($rsvr_id)
    {
        $this->rsrv_id = $rsrv_id;
        return $this;
    }


}