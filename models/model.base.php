<?php


abstract class Base
{

    public function exchangeArray($data)
    {
        foreach ($data as $key => $val) {
            if (property_exists($this, $key)) {
                $this->$key = $val;
            }
        }
        return $this;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


}