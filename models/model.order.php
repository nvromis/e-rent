<?php

class Order extends Base
{
    protected $order_id;
    protected $user_id_fk;
    protected $amount_from;
    protected $amount_to;
    protected $service_profit;
    protected $partner_money;
    protected $date_add;
    protected $status;
    protected $purse_from;
    protected $purse_to;
    protected $last_update;
    protected $edir_id_fk;

    /**
     * @return mixed
     */
    public function getAmountFrom()
    {
        return $this->amount_from;
    }

    /**
     * @param mixed $amount_from
     * @return Order
     */
    public function setAmountFrom($amount_from)
    {
        $this->amount_from = $amount_from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmountTo()
    {
        return $this->amount_to;
    }

    /**
     * @param mixed $amount_to
     * @return Order
     */
    public function setAmountTo($amount_to)
    {
        $this->amount_to = $amount_to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateAdd()
    {
        return $this->date_add;
    }

    /**
     * @param mixed $date_add
     * @return Order
     */
    public function setDateAdd($date_add)
    {
        $this->date_add = $date_add;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEdirIdFk()
    {
        return $this->edir_id_fk;
    }

    /**
     * @param mixed $edir_id_fk
     * @return Order
     */
    public function setEdirIdFk($edir_id_fk)
    {
        $this->edir_id_fk = $edir_id_fk;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * @param mixed $last_update
     * @return Order
     */
    public function setLastUpdate($last_update)
    {
        $this->last_update = $last_update;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param mixed $order_id
     * @return Order
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPartnerMoney()
    {
        return $this->partner_money;
    }

    /**
     * @param mixed $partner_money
     * @return Order
     */
    public function setPartnerMoney($partner_money)
    {
        $this->partner_money = $partner_money;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurseFrom()
    {
        return $this->purse_from;
    }

    /**
     * @param mixed $purse_from
     * @return Order
     */
    public function setPurseFrom($purse_from)
    {
        $this->purse_from = $purse_from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurseTo()
    {
        return $this->purse_to;
    }

    /**
     * @param mixed $purse_to
     * @return Order
     */
    public function setPurseTo($purse_to)
    {
        $this->purse_to = $purse_to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceProfit()
    {
        return $this->service_profit;
    }

    /**
     * @param mixed $service_profit
     * @return Order
     */
    public function setServiceProfit($service_profit)
    {
        $this->service_profit = $service_profit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserIdFk()
    {
        return $this->user_id_fk;
    }

    /**
     * @param mixed $user_id_fk
     * @return Order
     */
    public function setUserIdFk($user_id_fk)
    {
        $this->user_id_fk = $user_id_fk;
        return $this;
    }


}