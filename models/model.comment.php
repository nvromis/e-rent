<?php

class Comment extends Base
{

    protected $cmnt_id = null;
    protected $date;
    protected $message;
    protected $order_id_fk;
    protected $user_id;

    /**
     * @return null
     */
    public function getCmntId()
    {
        return $this->cmnt_id;
    }

    /**
     * @param null $cmnt_id
     * @return Comment
     */
    public function setCmntId($cmnt_id)
    {
        $this->cmnt_id = $cmnt_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return Comment
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return Comment
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderIdFk()
    {
        return $this->order_id_fk;
    }

    /**
     * @param mixed $order_id_fk
     * @return Comment
     */
    public function setOrderIdFk($order_id_fk)
    {
        $this->order_id_fk = $order_id_fk;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return Comment
     */
    public function setUserId(User $user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

}