<?php

class BaseController
{

    protected static $db = null;

    protected static $view = array('set' => array('layout' => true, 'view' => true, 'redirect' => null), 'json'=>array());

    public static function init()
    {
        self::$db = db::getInstance();
    }

    protected function enableLayout()
    {
        self::$view['set']['layout'] = true;
    }

    protected function disableLayout()
    {
        self::$view['set']['layout'] = false;
    }

    protected function enableView()
    {
        self::$view['set']['view'] = true;
    }

    protected function disableView()
    {
        self::$view['set']['view'] = false;
    }

    protected function setJson($key, $val)
    {
        self::$view['json'][$key] = $val;
    }

    protected function addJson($key, $val)
    {
        self::$view['json'] = array_merge(self::$view['json'], array($key => $val));
    }

    protected function addJsons($arr = array())
    {
        self::$view['json'] = array_merge(self::$view['json'], $arr);
    }

}

?>