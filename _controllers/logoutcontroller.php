<?php

class LogoutController extends BaseController
{
    public static function init()
    {
        parent::init();
    }

    public function index($params = array())
    {
        $view = &self::$view;
        if (Auth::hasIdentity()) {
            Auth::destroy();
            $view['set']['redirect'] = '/';
        }

        return $view;
    }

}
