<?php

class SignupController extends BaseController
{
    public static function init()
    {
        parent::init();
    }

    public function index($params = array())
    {
        $view = &self::$view;
        $ud = UserDao::getInstance();
        $set = &$view['set'];

        if (isset($_POST['email'])) {
            $set['view'] = false;
            $email = htmlspecialchars($_POST['email']);
            $pass = md5($_POST['password']);

            $json = &$view['json'];
            $json['message'] = 'Вы ввели неверный логин и/или пароль!';
            $json['success'] = false;
            $user = $ud->getUser($email);
            if (is_null($user)) {


                $user = array();
                $user['email'] = $email;
                $user['password'] = $pass;
                $ud->addUser((object)$user);
                $md = md5($email . rand(1, 100));

                $hash = array(
                    'hash' => $md,
                    'user_email' => $email,
                    'type' => 'activation'
                );
                $confDao = ConfirmationDao::getInstance();
                $confDao->addHash((object)$hash);

                $link = 'http://' . $_SERVER['SERVER_NAME'] . '/signup/activate/md/' . $md;
                $email_from = 'admin@obmen.ru';
                $email_to = $email;
                $subject = @trim(stripslashes(""));
                $body = 'Email: ' . $email . "\n\n" . 'Password: ' . $_POST['password'] . "\n\n" . 'Activation link: ' . $link . "\n\n";

                $success = @mail($email_to, $subject, $body, 'From: <' . $email_from . '>');
                $json['message'] = 'Вы успешно зарегистрировались в системе!<br> email:' . $email . '<br>password:' . $_POST['password'];
                $json['success'] = true;
            } else $json['message'] = 'Такой email уже есть в системе!';


        }
        return $view;
    }

    public function activate($params = array())
    {
        $view = &self::$view;
        $md = $params['md'];
        $confDao = ConfirmationDao::getInstance();
        $ch = $confDao->getByHash($md);
        $view['success'] = false;
        $view['message'] = 'Ошибка при активации!';

        if (!is_null($ch)) {
            $ud = UserDao::getInstance();
            $user = $ud->getUser($ch->user_email);

            $user->role = 'user';

            $ud->updateUser($user);
            $confDao->deleteUser($md);
            $view['success'] = true;
            $view['message'] = 'Ваш аккаунт успешно активирован!';
        }
        return $view;
    }

}
