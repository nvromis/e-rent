<?php

class GoodsController extends BaseController
{
    public static function init()
    {

        parent::init();
    }

    function index($params = array())
    {
        $view = &self::$view;
        $bd = BrandDao::getInstance();
        $cd = CategoryDao::getInstance();
        $view['brands'] = $bd->getAllBrands();
        $view['categories'] = $cd->getAllCategories();
        /*$cd = CommentDao::getInstance();
        $rd = ReserveDao::getInstance();
        $ud = UserDao::getInstance();

        $users = $ud->getAllUsers();
        $view['users'] = $users;

        $view['exdirs'] =$rd->getAllDirectionsByActive(1);

        $view['reserves'] = $rd->getReservesByActive(1);

        $view['comments'] =$cd->getLastComments();*/

        return $view;
    }

    public function api($params = array())
    {
        $view = &self::$view;
        $param=$_POST['p'];
        switch ($param){
            case 'mygoods':
                $this->disableView();
                $this->addJsons(GoodDao::getInstance()->getAllGoodsByUser(Auth::getIdentity()->id));
                break;
        }
        return $view;
    }
    public function history($params = array())
    {
        $view = &self::$view;
        return $view;
    }
    public function order($params = array())
    {
        $view = &self::$view;
        $this->disableView();
        $json = &$view['json'];
        $json['success']=false;
        $json['message']="";
        if ($_POST['good_id']!=null&&$_POST['from_date']!=null&&$_POST['to_date']!=null) {
            $good = GoodDao::getInstance()->getSimpleGood($_POST['good_id']);
            $id_ll = $good->user_id;
            $landlord = UserDao::getInstance()->getUserById($id_ll);
            if ($landlord->email!=Auth::getIdentity()->email) {
                $ord = array(
                    'from_date' => $_POST['from_date'],
                    'to_date' => $_POST['to_date'],
                    'landlord' => $landlord->email,
                    'tenant' => Auth::getIdentity()->email,
                    'goods_id' => $_POST['good_id'],
                    'status' => 'new'
                );
                $od = OrderDao::getInstance();
                $id_ord = $od->addOrder((object)$ord);
                $json['success'] = true;
                $json['message'] = "Заявка успішно створена! Для того щоб залишити відгук запишіть код заявки " . $id_ord;
            }else
            {
                $json['message']="Самому в себе не можна арендувати!";
            }
        }else
        {
        $json['message']="Помилка!";
        }

        return $view;
    }
    public function add($params = array())
    {

        $view = &self::$view;
        $bd = BrandDao::getInstance();
        $cd = CategoryDao::getInstance();
        $view['brands'] = $bd->getAllBrands();
        $view['categories'] = $cd->getAllCategories();
        $db_good = GoodDao::getInstance();
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
            $desc = $_POST['description'];
            $price = $_POST['price'];
            $cat_id = $_POST['category_id'];
            $brand_id = strtoupper($_POST['brand_id']);
            $bid = BrandDao::getInstance()->getBrandByName($brand_id);
            if (is_null($bid)){
               $bid = BrandDao::getInstance()->addBrand((object)array('brand'=>$brand_id));
                $bid = BrandDao::getInstance()->getBrandByName($brand_id);
            }

            $g = array(
                'name'=>$_POST['name'],
                'description'=>$_POST['description'],
                'price'=>$_POST['price'],
                'logo'=>null,
                'user_id'=>Auth::getIdentity()->id,
                'category_id'=>$_POST['category_id'],
                'brand_id'=>$bid->brand_id,
            );
            $id_g = $db_good->addGood((object)$g);

        }

        $error = false;
        $files = array();

        $uploaddir = APPLICATION_PATH . 'www/img/good_' . $id_g . '/';

        if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);
        $allowedExtensions = array(
            'jpg',
            'jpeg',
            'png',
            'gif'
        );
            $i=0;
        $phot = "";
        foreach ($_FILES as $file) {
            $i++;
            $ext = explode('.', $file['name']);
            $ext = $ext[count($ext) - 1];
            if (in_array(strtolower($ext), $allowedExtensions) && $file['size'] <= 5000 * 1024) {
                $fn = 'image_' . $i . '.' . $ext;
                if (move_uploaded_file($file['tmp_name'], $uploaddir . $fn)) {
                    $files_name = '/img/good_' . $id_g . '/' . $fn;
                    //$pd = PhotoalbumDao::getInstance()->addPhoto($id_g,$files_name);
                    $phot=$files_name;//.'|'.$phot;
                   $success=true;
                    $good = $db_good->getSimpleGood($id_g);
                    $good->logo = $phot;
                    $db_good->updateGood($good);
                } else {
                    $error = true;
                    $success = false;
                }
            } else {
                $error = true;
                $success = false;
            }

        }


       // print $phot."vse okey";

        $view['success']=$success;
        $view['error']=$error;
        return $view;
    }

}
