<?php

class ProfileController extends BaseController
{
    public static function init(){
        parent::init();
    }
    public function index($params = array())
    {
        $view = &self::$view;
        if (Auth::hasIdentity()){
            $view['user']=Auth::getIdentity();
        }

        return $view;
    }
    public function edit($params = array())
    {
        $view = &self::$view;
        $this->disableView();
        $json = &$view['json'];

        $name = htmlspecialchars($_POST['fname']);
        $surname = htmlspecialchars($_POST['fsurname']);
        $patronymic = htmlspecialchars($_POST['fpatronymic']);
        $json['success']=false;
        $json['message']="";
        $user=Auth::getIdentity();
        if (md5($_POST['opassword'])==$user->password){
            if (md5($_POST['fpassword'])==md5($_POST['spassword'])){
                $user->name = $name;
                $user->surname=$surname;
                $user->patronymic=$patronymic;

                $user->password = md5($_POST['fpassword']);
                UserDao::getInstance()->updateUser($user);
                $json['success']=true;
                $json['message']="Профиль успешно изменен!";

            } else $json['message']="Пароли не совпадают!";
        } else $json['message']="Неверно указан старый пароль!";

        return $view;
    }

}
