<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" indent="yes"/>
    <xsl:template match="/"> 
	<rss xmlns:yandex="news.yandex.ru" version="2.0">
<channel>
<title>E-RENT</title>
<link>http://109.251.115.116</link>
<yandex:logo type="square">
http://109.251.115.116/images/home/logo.png
</yandex:logo>
<description/>

        <xsl:for-each select="data/item">
            <item>
				<title><xsl:value-of select="good_name"/></title>
	
				<link><xsl:value-of select="concat('http://109.251.115.116/catalog/good/id/', good_id)"/></link>
                <description><xsl:value-of select="good_description"/></description>
				<category><xsl:value-of select="category_name"/></category>
				<comments><xsl:value-of select="concat('http://109.251.115.116/catalog/good/id/', good_id)"/></comments>
            </item>
        </xsl:for-each>  
</channel>	
</rss>	
</xsl:template>
</xsl:stylesheet>
