<?php

class OrderController extends BaseController
{
    public static function init(){
        parent::init();
        $view = &self::$view;
        $cd = CategoryDao::getInstance();
        $view['categories']=$cd->getAllCategories();

        $bd = BrandDao::getInstance();
        $view['brands']=$bd->getAllBrands();

        $gd = GoodDao::getInstance();
        $view['price']=$gd->getMinMax();
    }
    function index($params = array())
    {
        $view = &self::$view;

        return $view;
    }
    public function messages($params = array()){
        $view = &self::$view;
        return $view;
    }

}
