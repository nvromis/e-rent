<?php

class FeedbackController extends BaseController
{
    public static function init(){
        parent::init();
        $view = &self::$view;
        $cd = CategoryDao::getInstance();
        $view['categories']=$cd->getAllCategories();

        $bd = BrandDao::getInstance();
        $view['brands']=$bd->getAllBrands();

        $gd = GoodDao::getInstance();
        $view['price']=$gd->getMinMax();

    }
    function index($params = array())
    {
        $view = &self::$view;
        return $view;
    }
    public function add($params = array()){
        $view = &self::$view;
        return $view;
    }
	
    public function order($params = array()){
        $view = &self::$view;
        return $view;
    }
	

}
