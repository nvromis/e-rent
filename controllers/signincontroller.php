<?php

class SigninController extends BaseController
{
    public static function init()
    {
        parent::init();
    }

    public function index($params = array())
    {
	
        $view = &self::$view;
        $set = &self::$view['set'];
        $ud = UserDao::getInstance();

        $pub = config::getInstance()->get('recaptcha')->public_key;
        $priv = config::getInstance()->get('recaptcha')->private_key;

        $cap = new ReCaptchaV2($pub, $priv);



        if (isset($_POST['email'])){
           /* if (isset($_POST['g-recaptcha-response'])&&$_POST['g-recaptcha-response']!=null){
                $recap = $_POST['g-recaptcha-response'];
                $resp=$cap->verify($recap);

                if ($resp->isValid()){*/
                    $email = htmlspecialchars($_POST['email']);
                    $pass = md5($_POST['password']);
                    $user = $ud->getUserByEmailAndPassword($email, $pass);

                    $view['message'] = 'Вы ввели неверный логин и/или пароль!';
                    $view['status'] = false;
                    if (!is_null($user)) {
                        $view['message'] = 'Вы успешно вошли в систему! Ожидайте перенаправления!';
                        $view['status'] = true;
                        Auth::setIdentity($user);

                        $set['redirect'] = '/';
                    }
               /* }
           } else {$set['redirect'] = '/signin';}*/
        }
        //$view['captcha'] = $cap->getHtml();
        return $view;
    }

}
