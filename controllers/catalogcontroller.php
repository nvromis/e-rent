<?php

class CatalogController extends BaseController
{
    public static function init(){
        parent::init();
        $view = &self::$view;
        $cd = CategoryDao::getInstance();
        $view['categories']=$cd->getAllCategories();

        $bd = BrandDao::getInstance();
        $view['brands']=$bd->getAllBrands();

        $gd = GoodDao::getInstance();
        $view['price']=$gd->getMinMax();

    }
	
    function index($params = array())
    {
        $view = &self::$view;
        $gd = GoodDao::getInstance();
        $view['goods'] = $gd->getAllGoods();
        $view['top_goods'] = $gd->getTopGoods();
        $view['action']='get';
		
		$xmlDoc = new DOMDocument('<?xml-stylesheet type="text/xsl" href="rss.xsl"?>');
		$xmlDoc->loadXML(XMLSerializer::generateValidXmlFromArray($view['goods']));
		//$xml->save('rss.xml');
		
		 $xslDoc = new DOMDocument();
   $xslDoc->load("rss.xsl");

   //$xmlDoc = new DOMDocument();
   //$xmlDoc->load("rss.xml");

   $proc = new XSLTProcessor();
   $proc->importStylesheet($xslDoc);
   
   $xslDoc2 = new DOMDocument();
   $xslDoc2->loadXML($proc->transformToXML($xmlDoc));
   $xslDoc2->save('rss.xml');
		

        return $view;
    }
	
    public function add($params = array()){
        $view = &self::$view;
        return $view;
    }
	
    public function order($params = array()){
        $view = &self::$view;
        return $view;
    }
	
	public function good($params = array()){
        $view = &self::$view;
        $gd = GoodDao::getInstance();
        $good=$gd->getGood($params['id']);

        $view['good']=$good;
        $view['is_free']=$gd->isFree($good->good_id);
        $view['other_goods']=$gd->getGoodsByUser($good->user_id);
        $view['similar_goods']=$gd->getAllGoodsByCategory($good->category_id,4);
        return $view;
    }
    public function get($params = array()){
        $view = &self::$view;
        $gd = GoodDao::getInstance();
        if (isset($params['category'])) {
            $view['goods'] = $gd->getAllGoodsByCategory($params['category']);
            $view['top_goods'] = $gd->getTopGoodsByCategory($params['category']);
        }
        if (isset($params['brand'])) {
            $view['goods'] = $gd->getAllGoodsByCategory($params['brand']);
            $view['top_goods'] = $gd->getTopGoodsByCategory($params['brand']);
        }
        if (isset($_POST['q'])) {
            $word=htmlspecialchars($_POST['q']);
            $view['goods'] = $gd->searchGoods($word);
            $view['top_goods'] =null;// $gd->searchTopGoods($word);
            $view['action']='get';
        }
        return $view;
    }
	public function api($params = array()){
       // $view = &self::$view;
        $this->disableView();

       /* $view['api_data']=array('type' => 'success',
            'goods' => array('id' => 1,
                'name' => 'sd',
                'description' => 'ttt',
                'price' => 2.2,
                'new' => true,
                'category' => 'ttt',
                'brand' => 'Nike',
                'img' => 'http://shmot.com.ua/upload/resize_cache/iblock/9f2/400_400_0/9f2c027b0a46577a526b141d323966b8.jpg'
            )
        );

        return $view;*/
        $view = &self::$view;

        $view = &self::$view;
        $param=$_POST['p'];
		
        switch ($param){
            case 'mygoods':
                $this->disableView();
                $this->addJsons(array('goods' => GoodDao::getInstance()->getAllGoodsByUser(Auth::getIdentity()->id),
									  'schema' => 'mygoods.xsd'));
                break;
            case 'ordersforme':
                $this->disableView();
                $this->addJsons(array('goods'=>OrderDao::getInstance()->getOrdersForMe(Auth::getIdentity()->email),
									  'schema' => 'ordersforme.xsd'));
                break;
            case 'myorders':
                $this->disableView();
                $this->addJsons(array('goods'=>OrderDao::getInstance()->getMyOrders(Auth::getIdentity()->email),
									  'schema' => 'ordersforme.xsd'));
                break;
        }
        return $view;
    }
	




}
?>
