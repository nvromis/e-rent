<?php

class ProfileController extends BaseController
{
    public static function init(){
        parent::init();
    }
    public function index($params = array())
    {
        $view = &self::$view;
        if (Auth::hasIdentity()){
            $view['user']=Auth::getIdentity();
        }

        return $view;
    }
    public function edit($params = array())
    {
        $view = &self::$view;
        $this->disableView();
        $api_data = &$view['api_data'];

        $name = htmlspecialchars($_POST['fname']);
        $surname = htmlspecialchars($_POST['fsurname']);
        $patronymic = htmlspecialchars($_POST['fpatronymic']);
        $api_data['success']=false;
        $api_data['message']="";
        $user=Auth::getIdentity();
        if (md5($_POST['opassword'])==$user->password){
            if (md5($_POST['fpassword'])==md5($_POST['spassword'])){
                $user->name = $name;
                $user->surname=$surname;
                $user->patronymic=$patronymic;

                $user->password = md5($_POST['fpassword']);
                UserDao::getInstance()->updateUser($user);
                $api_data['success']=true;
                $api_data['message']="Профіль успішно змінено!";

            } else $api_data['message']="Паролі не співпадають!";
        } else $api_data['message']="Неправильно вказаний пароль!";
		
		$api_data['schema']='statusmsg.xsd';

        return $view;
    }

}
